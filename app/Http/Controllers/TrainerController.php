<?php

namespace App\Http\Controllers;

use App\Trainer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreTrainerRequest;
//php artisan make:model Trainer --migration  Crear Modelo y migracion

class TrainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     
        $request->user()->authorizeRoles(['admin','user']);
    	$trainers = Trainer::all(); //Consulta todos los datos y muestralos
        return view('trainers.index', compact('trainers'));

        //   return 'Hola desde el controlador resource';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('trainers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTrainerRequest $request)
    {
        $trainer = new Trainer(); // se crea una instancia del modelo
        if($request->hasFile('avatar')){

            $file = $request->file('avatar');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/', $name);
          }

        $trainer->name = $request->input('name'); //se accede al nombre 
        $trainer->avatar = $name; // se asigna el atributo avatar de la variable name
        $trainer->slug = $request->input('slug');
        $trainer->description = $request->input('description'); // Se asigna el atributo description a la variable name
        $trainer -> save(); // se almacena un nuevo recurso dentro de la BD
        return redirect()->route('trainers.index',[$trainer])->with('status','Entrenador Creado Correctamente');

       // return $request->input('name'); //Un atributo en especifico
      //  return $request->all(); Obtener todos los datos enviados por el usuario
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Trainer $trainer) //Usando implicit bildind
    {
     // $trainer = Trainer::where('slug','=',$slug)->firstOrfail();

        // $trainer = Trainer::find($id); //Busca informacion por id
      //  return $trainer; //Regresa la informacion que busco por el id
        return view('trainers.show', compact('trainer')); //Compact comparte la informacion con las vistas
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainer $trainer)
    {
        return view('trainers.edit', compact('trainer'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trainer $trainer)
    {
        //Fill() se encarga de actualizar los datos  $trainer->fill($request->all());
     $trainer->fill($request->except('avatar'));
     if($request->hasFile('avatar')){
        $file = $request->file('avatar');
        $name = time().$file->getClientOriginalName();
        $trainer->avatar=$name;
        $file->move(public_path().'/images/', $name);
      }
     $trainer->save();
     return redirect()->route('trainers.show',[$trainer])->with('status','Entrenador Actualizado Correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trainer $trainer)
    {
      

        $file_path = public_path().'/images/'.$trainer->avatar;
        \File::delete($file_path);
        $trainer->delete();
        return redirect()->route('trainers.index',[$trainer])->with('status','Entrenador Eliminado Correctamente');
    }
}
