@extends('layouts.app')


@section('title','Trainers Create')

@section('content')

@include('common.errors')

@include('common.success')

@include('trainers.form')

@endsection 